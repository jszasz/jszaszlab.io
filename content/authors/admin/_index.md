---
# Display name
name: Juraj Szász

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Molecular Biology Alumnus & Open Source Enthusiast

# Organizations/Affiliations
organizations:
#- name: Stanford University
#  url: ""

# Short bio (displayed in user profile at end of posts)
#bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Evolutionary Biology
- Bioinformatics
- Python

education:
  courses:
  - course: Master's Degree in Applied Biology
    institution: University of ss. Cyril and Methodius in Trnava
    year: 2017
  - course: Bachelor's Degree in Molecular Biology
    institution: Comenius University in Bratislava
    year: 2010

# social/academic networking
# for available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   for an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # for a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/jszasz
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/jurajszasz
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=siwtmxoaaaaj
- icon: github
  icon_pack: fab
  link: https://github.com/sars1492
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/jszasz
- icon: academia
  icon_pack: ai
  link: https://independent.academia.edu/jurajszasz3
- icon: mendeley
  icon_pack: ai
  link: https://www.mendeley.com/profiles/juraj-szsz/
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0001-6875-7999
## link to a pdf of your resume/cv from the about widget.
# to enable, copy your resume/cv to `static/files/cv.pdf` and uncomment the lines below.  
- icon: cv
  icon_pack: ai
  link: files/juraj-szasz-cv.pdf

# enter email to display gravatar (if gravatar enabled in config)
email: ""
  
# organizational groups that you belong to (for people widget)
#   set this to `[]` or comment out if you are not using people widget.  
user_groups:
- researchers
- visitors
---

Juraj Szász <span style="color:grey">/saːs/</span> is an independent researcher
in molecular biology and bioinformatics. His primary research interests is
evolutionary biology, and he is an active contributor to the
[Biopython](https://biopython.org) Project, which provides an Open Source
Python-based framework for bioinformatics. He also collaborates as a volunteer
in various biology-related research activities.
