---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Scientometric Analysis of the Faculty of Natural Sciences University of Ss. Cyril and Methodius in Trnava"
authors: [admin]
date: 2017-03-24T00:00:01+01:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2017-03-24T00:00:00+01:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["7"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "The purpose of scientometric analysis is to provide qualitative and quantitative evaluation
of the scientific work. It is being performed by statistical analysis of the publishing
activity—addressing individual researchers, institutions or even whole countries. The
quality of a scientific publication is derived from the number of citations. The amount of
references to a paper determines its popularity, quality and its impact on scientific progress.
The goal of this thesis is to carry out qualitative and quantitative analysis of the publishing
activity of the academic staff members affiliated to Faculty of Natural Science, University
of Ss. Cyril and Methodeus in Trnava during 2000–2016."

# Summary. An optional shortened abstract.
summary: ""

tags: ["bibliometry", "scientometry", "impact factor", "citation index", "evaluation of scientific work"]
categories: []
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: scientometric-analysis-of-the-faculty-of-natural-sciences-university-of-ss-cyril-and-methodius-in-trnava.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides: scientometric-analysis-of-the-faculty-of-natural-sciences-university-of-ss-cyril-and-methodius-in-trnava-slides.pdf
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
