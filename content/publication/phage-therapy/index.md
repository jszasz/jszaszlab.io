---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Phage Therapy"
authors: [admin]
date: 2010-04-24T00:00:01+01:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2010-04-24T00:00:00+01:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["7"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Phage therapy is use of bacteriophages, native bacterial parasites for therapeutic purpose.
Multiresistant bacteria such as MRSA and VRE is big problem of modern medicine and the
phage therapy is capable effectively treat the infections. The aim of the study is brief
overview about phage therapy: history, benefits, disadvantages and future prospects."

# Summary. An optional shortened abstract.
summary: ""

tags: ["bacteriophage", "pathogen", "life cycle", "host range"]
categories: []
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: phage-therapy.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
