+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Technical Assistant"
  company = "Nové Mesto nad Váhom District Court"
  company_url = ""
  location = "Nové Mesto nad Váhom, Slovakia"
  date_start = "2018-11-01"
  date_end = "2019-03-31"
  description = """
  Responsibilities include:
  
  * Recording of the Trial
  * IT Support 
  * Technical Support
  """

[[experience]]
  title = "Medical Laboratory Technician"
  company = "Alpha Medical s.r.o"
  company_url = "https://www.alphamedical.sk/"
  location = "Galanta, Slovakia"
  date_start = "2018-02-01"
  date_end = "2018-03-31"
  description = """
  Responsibilities included:
  
  * Recieving and processing of biological material (blood, serum, CSF)
  * Analysis of biological material (blood test, bleeding test)
  """


[[experience]]
  title = "Medical Laboratory Technician"
  company = "HPL s.r.o."
  company_url = ""
  location = "Bratislava, Slovakia"
  date_start = "2008-11-01"
  date_end = "2009-01-31"
  description = """
  Responsibilities included:
  
  * Recieving and processing of biological material (serum)
  * Analysis of biological material (antibodies, ELISA)
  """


+++
